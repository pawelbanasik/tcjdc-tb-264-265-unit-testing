package com.pawelbanasik;

import static org.junit.Assert.*;

import org.junit.Test;

public class BankAccountTest {

	@Test
	public void deposit() {
		BankAccount account = new BankAccount("Tim", "Buchalka", 1000, BankAccount.CHECKING);
		double balance = account.deposit(200, true);
		assertEquals(1200, balance, 0);
		assertEquals(1200, account.getBalance(), 0);
	}

	@Test
	public void withdraw() {
		fail("This test has yet to be implemented.");

	}

	@Test
	public void getBalance_deposit() {
		BankAccount account = new BankAccount("Tim", "Buchalka", 1000, BankAccount.CHECKING);
		account.deposit(200, true);
		assertEquals(1200, account.getBalance(), 0);
		
		
	}
	
	@Test
	public void getBalance_witdraw() {
		BankAccount account = new BankAccount("Tim", "Buchalka", 1000, BankAccount.CHECKING);
		account.withdraw(200, true);
		assertEquals(800, account.getBalance(), 0);
		
		
	}
	@Test
	public void isChecking_true() {
		BankAccount account = new BankAccount("Tim", "Buchalka", 1000, BankAccount.CHECKING);
		assertTrue(account.isChecking());
	}
	
}
